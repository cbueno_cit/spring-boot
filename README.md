# Running API

## Gerar a imagem

Para efetuar o build da imagem, rodar o comando:

`docker build --build-arg build_number=1 -t spring-boot:1 -t spring-boot:latest .`

Argumentos:

- build_number: número da build de nosso pipeline para termos todo o historico das imagens relacionado com a versão da nossa api.  
*Exemplo: 1.0.0-1, 1.0.0-2, etc*

## Executar

Para executar o container, rodar o comando:

`docker run --rm -p 8081:8080 spring-boot:1`

## Consumir

Exemplos:

`GET http://localhost:8081/api/v1/products?name=teste`

`GET http://localhost:8081/api/v1/products/1`

`POST http://localhost:8081/api/v1/products`

```json
{
    "bla": "bla"
}
```

`PUT http://localhost:8081/api/v1/products/1`

```json
{
    "bla": "bla"
}
```
