package com.example.demo.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.example.demo.entities.Product;

@RestController
@RequestMapping("api/v1/products")
public class DemoController {

    @GetMapping()
	@ResponseStatus(HttpStatus.OK)
	public static String get(
		@RequestParam(value = "name", defaultValue = "teste") String name, 
		@RequestParam(value = "bla", defaultValue = "testando") String bla) {
		return String.format("%s %s", name, bla);
	}

	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public static String get(@PathVariable Integer id) {
		return id.toString();
	}

	@PostMapping()
	@ResponseStatus(HttpStatus.CREATED)
	public static String post(@RequestBody Product teste) {
		return String.format("%s %s", teste.bla, teste.teste);
	}

	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public static String put(@PathVariable Integer id, @RequestBody Product teste) {
		return String.format("%s - %s %s", id.toString(), teste.bla, teste.teste);
	}
}
