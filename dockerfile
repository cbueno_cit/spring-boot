FROM openjdk:15-alpine

ARG build_number
ENV BUILD_NUMBER=$build_number

WORKDIR /usr/src/myapp

COPY . .

RUN ./gradlew -P buildNumber=$build_number build

EXPOSE 8080

CMD java -jar build/libs/demo-1.0.0-$BUILD_NUMBER.jar